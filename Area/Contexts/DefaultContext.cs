﻿using Area.Models;
using Microsoft.EntityFrameworkCore;

namespace Area.Contexts
{
    public sealed class DefaultContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<StudentMark> StudentMarks { get; set; }
        public DbSet<Subject> Subjects { get; set; }

        public DefaultContext() 
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=AreaDb;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FacultySubject>()
                .HasKey(t => new {t.FacultyId, t.SubjectId});
            modelBuilder.Entity<FacultySubject>()
                .HasOne(s => s.Subject)            /// у предмета 
                .WithMany(s => s.FacultySubjects) // много факультета
                .HasForeignKey(sc => sc.SubjectId); // связаны с предмет ID
            modelBuilder.Entity<FacultySubject>()
                .HasOne(s => s.Faculty)  // у Факультета 
                .WithMany(c => c.FacultySubjects) // много предметов
                .HasForeignKey(c => c.FacultyId); // связанных с факультет ID



            modelBuilder.Entity<SubjectUser>()
                .HasKey(t => new {t.UserId, t.SubjectId});
            modelBuilder.Entity<SubjectUser>()
                .HasOne(u => u.User) // у юзера 
                .WithMany(s => s.SubjectUsers) // много предметов
                .HasForeignKey(u => u.UserId); // связанных с юзер ID
            modelBuilder.Entity<SubjectUser>()
                .HasOne(s => s.Subject) // у премдета 
                .WithMany(s => s.SubjectUsers) // много юзера
                .HasForeignKey(s => s.SubjectId); // связанных с предмет ID 


            modelBuilder.Entity<UserMarks>()
                .HasKey(m => new {m.UserId, m.StudentMarkId});
            modelBuilder.Entity<UserMarks>()
                .HasOne(s => s.User) // У юзера 
                .WithMany(s => s.UserMarkses) // Много оценок 
                .HasForeignKey(sc => sc.UserId); // связанные с юзерID 
            modelBuilder.Entity<UserMarks>()
                .HasOne(s => s.StudentMark) // у оценок 
                .WithMany(s => s.UserMarkses) // Много юзеров
                .HasForeignKey(sc => sc.StudentMarkId); // связанные с оценниками Id 

            modelBuilder.Entity<FacultyUser>()
                .HasKey(f => new {f.FacultyId, f.UserId});
            modelBuilder.Entity<FacultyUser>()
                .HasOne(f => f.User) // У юзера 
                .WithMany(f => f.FacultyUsers) // Много факультета 
                .HasForeignKey(f => f.UserId); // Свящаннык с юзер ID 
            modelBuilder.Entity<FacultyUser>()
                .HasOne(f => f.Faculty) // У факультета 
                .WithMany(f => f.FacultyUsers) // Много юзера 
                .HasForeignKey(f => f.FacultyId); // Связаные с факультет ID
        }
    }
}