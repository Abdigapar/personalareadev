﻿using System;
using System.Linq;
using Area.Contexts;
using Area.Models;

    public class Admin
    {
        readonly DefaultContext _context = new DefaultContext();
        CrudFaculty crudFaculty = new CrudFaculty();

    public void AdminOption()
    {
        User loggedUser = null;
        var again = true;
        var again2 = true;
        var again3 = true;
        var inputLogin = "";

            while (again)
            {
                Console.WriteLine("Введите логин");
                inputLogin = Console.ReadLine();

                var user = _context.Users.FirstOrDefault(c => c.Login == inputLogin);

                if (user == null)
                {
                    Console.WriteLine("Введите логин заново");
                }
                else if (user.UserType != UserType.Admin)
                {
                    Console.WriteLine("Введенный логин не имеет админских прав");
                }
                else
                {
                    again = false;
                    loggedUser = user;
                }
            }

            while (again2)
            {
                Console.WriteLine("Введите пароль");
                var inputPassword = Console.ReadLine();

                var checkPassword = _context.Users.FirstOrDefault(c => c.Password == inputPassword && c.Login == inputLogin);

                if (checkPassword == null)
                    Console.WriteLine("Введите пароль заново");
                else
                {
                    again2 = false;
                }
            }

        Console.WriteLine($"Welcome {loggedUser.UserType} - {loggedUser.FirstName}");

        while (again3)
        {
            Console.WriteLine("Выберите операцию: \n1) Функционал пользователей \n2) Функционал факультета" +
                              "\n3) Функционал предметов");

            var input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    CrudUser.OperationUser();
                    break;
                case "2":
                    CrudFaculty.OperationFaculty();
                    break;
                case "4":
                    CrudSubject.OperationSubject();
                    break;
                default:
                    Console.WriteLine("Не правильная операция");
                    break;
            }

                Console.WriteLine("Logout? y/n");
                var input1 = Console.ReadLine();
                if (input1 == "y".ToLower() || input1 != "n".ToLower())
                {
                    again3 = false;
                }
            }
        }
    }

}
