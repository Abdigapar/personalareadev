﻿using System;
using System.Linq;
using Area.Contexts;
using Area.Models;

namespace Area.CRUD
{
    public class CrudFaculty
    {
        public static DefaultContext Context = new DefaultContext();

        public static void OperationFaculty()
        {
            Console.WriteLine("Выберите операцию: \n1)  Список всех факультетов \n2)Добавить факультет " +
                              "\n3) Изменить факультет \n4) Удалить факультет");
            var input = Console.ReadLine();
            switch (input)
            {
                case "2":
                    GetAllFaculty();
                    break;
                case "8":
                    AddFaculty();
                    break;
                case "9":
                    UpdateFaculty();
                    break;
                case "10":
                    DeleteFaculty();
                    break;
                default:
                    Console.WriteLine("Не правильная операция");
                    break;
            }
        }

        private static void GetAllFaculty()
        {
            Console.WriteLine("Список факультетов");
            var allFaculty = Context.Faculties.ToList();

            foreach (var f in allFaculty)
            {
                Console.WriteLine($"{f.Id} | {f.Name}");
            }
        }

        private static void AddFaculty()
        {
            Console.WriteLine("Введите имя факультета");
            var name = Console.ReadLine();

            var newFaculty = new Faculty { Name = name };

            Context.Add(newFaculty);
            Context.SaveChanges();
        }

        private static void UpdateFaculty()
        {
            Console.WriteLine("Введите ID факультета");
            var id = Convert.ToInt32(Console.ReadLine());

            var searchId = Context.Faculties.FirstOrDefault(c => c.Id == id);

            Console.WriteLine(searchId == null
                ? "Извините, факультет с таким ID не существует"
                : $"Факультет - {searchId.Name}");

            Console.WriteLine("Введите имя факультета");
            var name = Console.ReadLine();

            var updateFaculty = new Faculty { Name = name };

            Context.Update(updateFaculty);
            Context.SaveChanges();
        }

        private static void DeleteFaculty()
        {
            Console.WriteLine("Введите ID факультета");
            var id = Convert.ToInt32(Console.ReadLine());

            var searchId = Context.Faculties.FirstOrDefault(c => c.Id == id);

            Console.WriteLine(searchId == null
                ? "Извините, факультет с таким ID не существует"
                : $"Факультет - {searchId.Name}");

            Context.Remove(searchId ?? throw new InvalidOperationException());
            Context.SaveChanges();
        }
    }
}
