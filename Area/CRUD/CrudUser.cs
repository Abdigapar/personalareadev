﻿using System;
using System.Linq;
using Area.Contexts;
using Area.Models;

namespace Area.CRUD
{
    public class CrudUser
    {
        public static DefaultContext Context = new DefaultContext();

        public static void OperationUser()
        {
            Console.WriteLine("Выберите операцию: \n1) Список всех учителей \n2) Список всех студентов " +
                              "\n3) Добавить пользователя \n4) Изменить учетную запись " +
                              "\n5) Удалить учетную запись");
            var input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    GetAllTeacher();
                    break;
                case "2":
                    GetAllStudent();
                    break;
                case "3":
                    AddUser();
                    break;
                case "4":
                    UpdateUser();
                    break;
                case "5":
                   DeleteUser();
                    break;
                default: Console.WriteLine("Не правильная операция");
                    break;
            }
        }

        private static void GetAllTeacher()
        {
            var allTeacher = Context.Users.Where(c => c.UserType == UserType.Teacher);
            Console.WriteLine("All Teachers");
            foreach (var user in allTeacher)
            {
                Console.WriteLine($"{user.Id} | {user.FirstName} - {user.LastName} - {user.UserType}");
            }
        }

        private static void GetAllStudent()
        {
            var allStudents = Context.Users.Where(c => c.UserType == UserType.Student);
            Console.WriteLine("All Students");
            foreach (var user in allStudents)
            {
                Console.WriteLine($"{user.Id} | {user.FirstName} - {user.LastName} - {user.UserType}");
            }
        }

        private static void AddUser()
        {
            Console.WriteLine("Введите имя пользователя");
            var firstName = Console.ReadLine();

            Console.WriteLine("Введите фамилию пользователя");
            var lastName = Console.ReadLine();

            Console.WriteLine("Введите логин");
            var login = Console.ReadLine();

            Console.WriteLine("Введите пароль");
            var password = Console.ReadLine();

            Console.WriteLine("Выберите роль  1 - Учитель, 2 - Студент");
            var userType = (UserType)Enum.Parse(typeof(UserType), Console.ReadLine());

            var user = new User()
            {
                FirstName = firstName,
                LastName = lastName,
                Login = login,
                Password = password,
                UserType = userType
            };

            Context.Users.Add(user);
            Context.SaveChanges();
        }

        private static void UpdateUser()
        {
            Console.WriteLine("Введите ID пользователя");

            var id = Convert.ToInt32(Console.ReadLine());

            var searchId = Context.Users.FirstOrDefault(c => c.Id == id);

            Console.WriteLine(searchId == null
                ? "Извините, пользователь с таким ID не существует"
                : $"Пользователь - {searchId.FirstName}");

            Console.WriteLine("Введите имя пользователя");

            var firstName = Console.ReadLine();

            Console.WriteLine("Введите фамилию студента");

            var lastName = Console.ReadLine();

            Console.WriteLine("Введите логин");

            var login = Console.ReadLine();

            Console.WriteLine("Введите пароль");

            var password = Console.ReadLine();

            if (searchId != null)
            {
                searchId.FirstName = firstName;
                searchId.LastName = lastName;
                searchId.Login = login;
                searchId.Password = password;

                Context.Update(searchId);
            }

            Context.SaveChanges();

            Console.WriteLine("Изменение сохранены");
        }

        private static void DeleteUser()
        {
            Console.WriteLine("Введите ID пользователя");

            var id = Convert.ToInt32(Console.ReadLine());

            var searchId = Context.Users.FirstOrDefault(c => c.Id == id);

            Console.WriteLine(searchId == null
                ? "Извините, пользователь с таким ID не существует"
                : $"Пользователь - {searchId.FirstName}");

            Context.Remove(searchId ?? throw new InvalidOperationException());

            Context.SaveChanges();

            Console.WriteLine("Пользователь удален");
        }
    }
}