﻿namespace Area.CRUD
{
    using System;
    using System.Linq;
    using Contexts;
    using Models;
    public class CrudSubject
    {
        public static DefaultContext Context = new DefaultContext();

        public static void OperationSubject()
        {
            Console.WriteLine("Выберите операцию: \n1) Список всех предметов \n2) Добавить предмет" +
                              "\n3) Обновить предмет \n4) Удалить предмет");

            var input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    GetAllSubject();
                    break;
                case "2":
                    AddSubject();
                    break;
                case "3":
                    UpdateSubject();
                    break;
                case "4":
                    DeleteSubject();
                    break;
                default:
                    Console.WriteLine("Не правильная операция");
                    break;
            }
        }

        private static void GetAllSubject()
        {
            Console.WriteLine("Список предметов");
            var allSubject = Context.Subjects.ToList();

            foreach (var s in allSubject)
            {
                Console.WriteLine($"{s.Id} | {s.Name}");
            }
        }

        private static void AddSubject()
        {
            Console.WriteLine("Введите имя предмета");
            var name = Console.ReadLine();

            var newSubject = new Subject { Name = name };

            Context.Add(newSubject);
            Context.SaveChanges();
        }

        private static void UpdateSubject()
        {
            Console.WriteLine("Введите ID предмета");
            var id = Convert.ToInt32(Console.ReadLine());

            var searchId = Context.Subjects.FirstOrDefault(c => c.Id == id);

            Console.WriteLine(searchId == null
                ? "Извините, предмет с таким ID не существует"
                : $"Предмет - {searchId.Name}");

            Console.WriteLine("Введите имя предмета");
            var name = Console.ReadLine();

            var updateFaculty = new Subject { Name = name };

            Context.Update(updateFaculty);
            Context.SaveChanges();
        }

        private static void DeleteSubject()
        {
            Console.WriteLine("Введите ID предмета");
            var id = Convert.ToInt32(Console.ReadLine());

            var searchId = Context.Subjects.FirstOrDefault(c => c.Id == id);

            Console.WriteLine(searchId == null
                ? "Извините, предмет с таким ID не существует"
                : $"Предмет - {searchId.Name}");

            Context.Remove(searchId ?? throw new InvalidOperationException());
            Context.SaveChanges();
        }
    }
}