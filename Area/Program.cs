﻿using System;

namespace Area
{
    class Program
    {
        static void Main(string[] args)
        {
            var again = true;

            while (again)
            {
                Console.WriteLine("Welcome to Personal Area");
                Console.WriteLine("Please enter \n1) Admin \n2) Teacher \n3) Student");
                var input = Console.ReadLine();

                switch (input)
                {
                    case "1":
                        var administrator = new Admin();
                        administrator.AdminOption();
                        break;
                    case "2":
                        var teacher = new Teacher();
                        break;
                    case "3":
                        var student = new Student();
                        //  student.StudentOption();
                        break;
                }

                Console.WriteLine("Хотите продолжить работу в личном кабинете: Y/N? ");
                var input1 = Console.ReadLine();
                if (input1 == "n".ToLower() || input1 != "y".ToLower())
                {
                    again = false;
                }
            }
            Console.Read();
        }
    }
}
