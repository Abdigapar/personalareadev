﻿using System.Collections.Generic;

namespace Area.Models
{
    public class User
    {
        public User()
        {
            SubjectUsers = new List<SubjectUser>();
            UserMarkses = new List<UserMarks>();
            FacultyUsers = new List<FacultyUser>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public UserType UserType { get; set; }
        public ICollection<FacultyUser> FacultyUsers { get; set; }
        public ICollection<SubjectUser> SubjectUsers { get; set; }
        public ICollection<UserMarks> UserMarkses { get; set; }
    }
}