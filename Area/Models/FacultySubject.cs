﻿namespace Area.Models
{
    public class FacultySubject
    {
        public int FacultyId { get; set; }
        public Faculty Faculty { get; set; }

        public int SubjectId { get; set; }
        public Subject Subject { get; set; }
    }
}