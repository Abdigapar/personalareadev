﻿namespace Area.Models
{
    public class FacultyUser
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int FacultyId { get; set; }
        public Faculty Faculty { get; set; }    

    }
}