﻿namespace Area.Models
{
    public class SubjectUser
    {
        public int UserId { get; set; }
        public User User { get; set; }

        public int SubjectId { get; set; }
        public Subject Subject { get; set; }
    }
}