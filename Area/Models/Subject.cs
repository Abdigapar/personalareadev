﻿using System.Collections.Generic;

namespace Area.Models
{
    public class Subject
    {
        public Subject()
        {
            FacultySubjects = new List<FacultySubject>();
            SubjectUsers = new List<SubjectUser>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<FacultySubject> FacultySubjects { get; set; }
        public ICollection<SubjectUser> SubjectUsers { get; set; }
    }
}