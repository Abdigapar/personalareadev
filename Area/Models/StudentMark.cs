﻿using System.Collections.Generic;

namespace Area.Models
{
    public class StudentMark
    {
        public StudentMark()
        {
            UserMarkses= new List<UserMarks>();
            Subjects = new List<Subject>();
        }

        public int Id { get; set; }
        public string Mark { get; set; }
        public ICollection<UserMarks> UserMarkses { get; set; }
        public ICollection<Subject> Subjects { get; set; }
    }
}