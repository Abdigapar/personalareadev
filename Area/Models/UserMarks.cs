﻿namespace Area.Models
{
    public class UserMarks
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public int StudentMarkId { get; set; }
        public StudentMark StudentMark { get; set; }
    }
}