﻿using System.Collections.Generic;

namespace Area.Models
{
    public class Faculty
    {
        public Faculty()
        {
            FacultySubjects = new List<FacultySubject>();
            FacultyUsers = new List<FacultyUser>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<FacultySubject> FacultySubjects { get; set; }
        public ICollection<FacultyUser> FacultyUsers { get; set; }
    }
}